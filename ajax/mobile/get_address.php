<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once '../../core/config_m.php';

$response_array['array_data'] = array();
$view_report = $mysqli_connect->query("SELECT * FROM tbl_address");

while ($data = $view_report->fetch_array()) {
	$list = array();

	$list["address_id"] = $data["address_id"];
	$list["address"] = $data["address"];

	array_push($response_array['array_data'], $list);
}

echo json_encode($response_array);
