<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once '../../core/config_m.php';

$user_id = $_REQUEST['user_id'];
$response_array['array_data'] = array();
$view_report = $mysqli_connect->query("SELECT * FROM tbl_report WHERE user_id='$user_id' AND status != 2");

while ($data = $view_report->fetch_array()) {
	$list = array();
	
	$view_attachments = $mysqli_connect->query("SELECT * FROM tbl_attachment WHERE report_id='$data[report_id]'");
	$a_data = $view_attachments->fetch_array();
	$img = explode("/", $a_data["slug"]);

	$list["report_id"] = $data["report_id"];
	$list["user_id"] = $data["user_id"];
	$list["user_name"] = getUserName($data["user_id"], 0);
	$list["status"] = $data["status"];
	$list["date_added"] = date("Y-m-d h:i A", strtotime($data["date_added"]));
	$list["description"] = $data["description"];
	$list["attachment"] = $img[3];

	array_push($response_array['array_data'], $list);
}

echo json_encode($response_array);
