<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require_once '../../core/config.php';

    $date = getCurrentDate();
    $response_array['array_data'] = array();
    $get_yr = $mysqli_connect->query("SELECT count(report_id) FROM tbl_report WHERE YEAR(date_added) = '".date("Y")."'");
    $get_mo = $mysqli_connect->query("SELECT count(report_id) FROM tbl_report WHERE MONTH(date_added) = '".date("m")."'");

    $total_yr = $get_yr->fetch_array();
    $total_mo = $get_mo->fetch_array();
    
    if ($total_yr && $total_mo){
        $response['res'] = 1;
        $response['total_yr'] = $total_yr[0];
        $response['total_mo'] = $total_mo[0];
    } else {
        $response['res'] = 0;
    }

    array_push($response_array['array_data'], $response);
    echo json_encode($response_array);
?>