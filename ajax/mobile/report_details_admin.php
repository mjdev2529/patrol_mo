<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once '../../core/config_m.php';

$response_array['array_data'] = array();
$view_report = $mysqli_connect->query("SELECT * FROM tbl_report");

while ($data = $view_report->fetch_array()) {
	$list = array();
	$img = explode("/", $data["attachment"]);

	$list["report_id"] = $data["report_id"];
	$list["user_id"] = $data["user_id"];
	$list["user_name"] = getUserName($data["user_id"], 0);
	$list["status"] = $data["status"];
	$list["date_added"] = $data["date_added"];
	$list["description"] = $data["description"];
	$list["attachment"] = $img[3];
	$list["priority"] = $data["priority"] == 1?"Low":($data["priority"] == 2?"Medium":"High");

	array_push($response_array['array_data'], $list);
}

echo json_encode($response_array);
