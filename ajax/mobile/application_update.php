<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


require_once '../../core/config_m.php';

//$data = json_decode(file_get_contents("php://input"));
$application_id = $_REQUEST['application_id'];
$status = $_REQUEST['status'];
$response_array['array_data'] = array();
if (isset($application_id) && isset($status)) {
	$application_id = $mysqli_connect->real_escape_string($application_id);
	$status = $mysqli_connect->real_escape_string($status);

	$update = $mysqli_connect->query("UPDATE tbl_application SET status = '$status', is_read = 0 WHERE application_id = '$application_id'");
	if ($update) {
		$response["response"] = 1;
	} else {
		$response["response"] = -1;
	}
}
array_push($response_array['array_data'], $response);
echo json_encode($response_array);
