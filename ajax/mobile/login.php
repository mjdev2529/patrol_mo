<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../core/config_m.php';

//$data = json_decode(file_get_contents("php://input"));
$username = $_REQUEST['username'];
$password = md5($_REQUEST['password']);
$idtoken = $_REQUEST['idtoken'];
$response_array['array_data'] = array();
if (isset($username) && isset($password)) {
	$username = $mysqli_connect->real_escape_string($username);
	$password = $mysqli_connect->real_escape_string($password);

	// count users
	$fetch_users = $mysqli_connect->query("SELECT *,count(user_id) as ctr FROM tbl_users WHERE username='$username' AND password='$password' /*AND role != 1*/");
	$data = $fetch_users->fetch_array();
	if ($data['ctr'] == 0 || $data["role"] == 1) {
		// cannot find account
		$response["response"] = -1;
	} else {
		if ($data['status'] == 1) {
			$response["response"] = -2;
		} else {
			$mysqli_connect->query("UPDATE tbl_users SET api_key='$idtoken' WHERE `user_id`='$data[user_id]'");
			if ($data['role'] == 1) { // for admin
				$response["name"] = $data['name'];
				$response["address"] = $data['address'];
				$response["contact_num"] = $data['contact_num'];
				$response["role"] = $data['role'];
				$response["status"] = $data['status'];
				$response["user_id"] = $data['user_id'];
				$response["response"] = 2;
			} else if($data["role"] == 0 && $data["username"] == $username && $data["password"] == $password) {
				$response["name"] = $data['name'];
				$response["address"] = $data['address'];
				$response["contact_num"] = $data['contact_num'];
				$response["role"] = $data['role'];
				$response["status"] = $data['status'];
				$response["user_id"] = $data['user_id'];
				$response["response"] = 1;
			}else{
								$response["response"] = 0;

			}
		}
	}
}
array_push($response_array['array_data'], $response);
echo json_encode($response_array);
