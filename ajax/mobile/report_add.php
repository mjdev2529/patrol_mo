<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../core/config_m.php';
require_once '../../core/function_helper.php';


//$data = json_decode(file_get_contents("php://input"));
$description = $_REQUEST['textPost'];
$user_id = $_REQUEST['user_id'];
$address = $_REQUEST['address'];
$priority = $_REQUEST['priority'];
$post_type = $_REQUEST['post_type'];
$date = date("Y-m-d H:i:s");
$response_array['array_data'] = array();
if (isset($description)) {
	$add_report = $mysqli_connect->query("INSERT INTO tbl_report SET `user_id` = '$user_id', `description` = '$description', `status` = 0, `date_added` = '$date', `address`='$address', priority='$priority'");
	if ($add_report) {
			$res = 0;
			$report_id = $mysqli_connect-> insert_id;
			foreach($_FILES['file']['name'] as $file => $name){
				// 	$description = $mysqli_connect->real_escape_string($description);
				$imgName = $user_id . "-" . date("mdyHis") . "-".$file.".jpg";
				$slugImg =  '../../assets/img/' . $imgName; // local
				$slug = "../assets/img/" . $imgName;

				$addAttachment = $mysqli_connect->query("INSERT INTO tbl_attachment SET report_id = '$report_id', slug = '$slug', date_added = '$date'");
				$add_report = $mysqli_connect->query("UPDATE tbl_report SET `attachment` = '$slug' WHERE report_id = '$report_id'");
                if($addAttachment){
					if (!file_exists($imgName)) {
						move_uploaded_file($_FILES['file']['tmp_name'][$file], $slugImg);
						$res += 1;
					}
				}else{
					$res = 0;
				}
			}

			// if($priority == 3){
			// 	$title = "A high priority report was added!";
			// 	$body = "A high priority report was added. Please review and respond immediately.";
			// 	// sendNotif($user_id, $title, $body, $conn);
			// 	$get_admin = $mysqli_connect->query("SELECT * FROM tbl_users WHERE role = 1");
			// 	while ($data = $get_admin->fetch_array()) {
			// 		sendNotif($data["api_key"], $title, $body);
			// 	}
			// }

		if($res != 0){
			$response["response"] = 1;
		}else{
			$response["response"] = -1;
		}
	} else {

		$response["response"] = -1;
	}
}
array_push($response_array['array_data'], $response);
echo json_encode($response_array);
