<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

session_start();
require_once '../../core/config_m.php';

//$data = json_decode(file_get_contents("php://input"));
$report_id = $_REQUEST['report_id'];
$description = $_REQUEST['description'];
$response_array['array_data'] = array();
if (isset($report_id) && isset($description)) {
	$report_id = $mysqli_connect->real_escape_string($report_id);
	$description = $mysqli_connect->real_escape_string($description);

	$update_report = $mysqli_connect->query("UPDATE tbl_report SET description = '$description' WHERE report_id = '$report_id'");
	if ($update_report) {
		$response["response"] = 1;
	} else {		
		$response["response"] = -1;
	}
}
array_push($response_array['array_data'], $response);
echo json_encode($response_array);
