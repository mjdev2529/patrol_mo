<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
require_once '../../core/config_m.php';

$report_id = $_REQUEST['report_id'];
$response_array['array_data'] = array();
$view_attachment = $mysqli_connect->query("SELECT * FROM tbl_attachment WHERE report_id='$report_id'");

while ($data = $view_attachment->fetch_array()) {
	$list = array();
    $img = explode("/", $data["slug"]);
	$list["slug"] = $img[3];

	array_push($response_array['array_data'], $list);
}

echo json_encode($response_array);
