<?php
	session_start();
	include '../../core/config.php';
	$filter = $_SESSION["role"] == 0 && $_POST["filter"]==1?" WHERE user_id = '".$_SESSION['uid']."'":($_SESSION["role"] == 0 && $_POST["filter"]!=0?" AND user_id = '".$_SESSION['uid']."'":"");
	
	if($_POST["filter"]==2){
		$s_filter = "WHERE status = 0";
	}else if($_POST["filter"]==3){
		$s_filter = "WHERE status = 1";
	}else if($_POST["filter"]==4){
		$s_filter = "WHERE status = 3";
	}else if($_POST["filter"]==5){
		$s_filter = "WHERE status = 2";
	}else{
		$s_filter = "";
	}

	$data = mysqli_query($conn,"SELECT * FROM tbl_report $s_filter $filter");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["report_id"] = $row["report_id"];
		$list["user"] = strtoupper(getUsername($conn, $row["user_id"], 0));
		$list["address"] = strtoupper($row["address"]);
		$list["status"] = $row["status"] == 1?"Approved":($row["status"] == 2?"Cancelled":($row["status"] == 3?"Resolved":"For Review"));
		$list["priority"] = $row["priority"] == 1?"<span class='text-dark'>Low</span>":($row["priority"] == 2?"<span class='text-warning'>Medium</span>":($row["priority"] == 3?"<span class='text-danger'>High</span>":"For Review"));
		$list["date_added"] = date("Y-m-d", strtotime($row["date_added"]));
		$list["description"] = $row["description"];
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>
