<?php
	session_start();
	include '../../core/config.php';
	$filter = $_SESSION["role"] == 0 && $_POST["filter"]==1?" WHERE user_id = '".$_SESSION['uid']."'":($_SESSION["role"] == 0 && $_POST["filter"]!=0?" AND user_id = '".$_SESSION['uid']."'":"");
	
	if($_POST["filter"]==2){
		$s_filter = "WHERE status = 0";
	}else if($_POST["filter"]==3){
		$s_filter = "WHERE status = 1";
	}else if($_POST["filter"]==4){
		$s_filter = "WHERE status = 2";
	}else if($_POST["filter"]==5){
		$s_filter = "WHERE status = 3";
	}else{
		$s_filter = "";
	}

	$data = mysqli_query($conn,"SELECT * FROM tbl_application $s_filter $filter");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["application_id"] = $row["application_id"];
		$list["user"] = strtoupper(getUsername($conn, $row["user_id"], 0));
		$list["application_type"] = getApplicationType($row["application_type"]);
		$list["status"] = $row["status"] == 1?"In-Progress":($row["status"] == 2?"Complete":($row["status"] == 3?"Cancelled":"Applied"));
		$list["stat"] =  $row["status"];
		$list["date_added"] = date("Y-m-d", strtotime($row["date_added"]));
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>
