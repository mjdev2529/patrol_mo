<?php
	session_start();
	include '../../core/config.php';
	$filter = $_SESSION["role"] == 0?" WHERE user_id = '".$_SESSION['uid']."'":"";
	$data = mysqli_query($conn,"SELECT * FROM tbl_users $filter");
	$response["data"] = array();
	$count = 1;
	while($row = mysqli_fetch_array($data)){
		$list = array();
		$list["count"] = $count++;
		$list["user_id"] = $row["user_id"];
		$list["name"] = $row["name"];
		$list["username"] = $row["username"];
		$list["role"] = $row["role"] == 1?"Admin":"Citizen";
		$list["hide"] = $row["status"] == 0?"style='display:none;'":"";
		$list["hide2"] = $row["status"] == 1?"style='display:none;'":"";
		array_push($response["data"], $list);
	}

	echo json_encode($response);

?>