<?php
    include '../core/config.php';
    session_start();
    $type = $_POST["type"];
    $typeID = $type == 0?$_POST["app_id"]:$_POST["report_id"];
    $typeC = $type == 0?"A":"R";

    if($type == 0){
        $app_id = $_POST["app_id"];
        $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_application WHERE application_id = '$app_id'"));
        if($data["status"] != 3){
?>
<div class="container-fluid">
<!-- Timelime example  -->
  <div class="row">
    <div class="col-md-12">
      <!-- The time line -->
      <div class="timeline">
        
        <?php if($data["date_added"]){ ?>
            <!-- timeline item -->
            <div>
            <i class="fas fa-circle bg-dark"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_added"]))?></span>
                <h3 class="timeline-header no-border"><b><?=getApplicationType($data['application_type'])?></b> was submitted</h3>
            </div>
            </div>
            <!-- END timeline item -->
        <?php }  ?>
            
        <?php if($data["date_approved"]){ ?>
            <!-- timeline item -->
            <div>
            <i class="fas fa-circle bg-dark"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_approved"]))?></span>
                <h3 class="timeline-header no-border"><b><?=getApplicationType($data['application_type'])?></b> was approved, And is now being processed.</h3>
            </div>
            </div>
            <!-- END timeline item -->
        <?php }  ?>

        <?php if($data["date_completed"]){ ?>
            <!-- timeline item -->
            <div>
                <i class="fas fa-circle bg-dark text-sm"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_completed"]))?></span>
                    <h3 class="timeline-header no-border"><b><?=getApplicationType($data['application_type'])?></b> is now completed. You may now proceed to the barangay hall and get it.</h3>
                </div>
            </div>
            <!-- END timeline item -->
            <div class="time-label">
                <span class="bg-success">
                    Completed
                </span>
            </div>
        <?php }else{  ?>
            <div class="time-label">
                <span class="bg-warning">
                    In-progress
                </span>
            </div>
        <?php } ?>
      </div>
    </div>
    <!-- /.col -->
  </div>
</div>
<?php
        }else{
            echo "<h3 class='text-center text-danger'>Cancelled</h3>";
        }
    }else{
        $report_id = $_POST["report_id"];
        $data = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_report WHERE report_id = '$report_id'"));
        $priority = $data["priority"] == 1?"<span class='text-dark'>Low</span>":($data["priority"] == 2?"<span class='text-warning'>Medium</span>":($data["priority"] == 3?"<span class='text-danger'>High</span>":"For Review"));
        
        if($data["status"] != 3){
?>
<div class="container-fluid">
<!-- Timelime example  -->
  <div class="row">
    <div class="col-md-12">
      <!-- The time line -->
      <div class="timeline">
          
        <div class="time-label">
            <span class="bg-light">
               Priority: <?=$priority?>
            </span>
        </div>
        
        <?php if($data["date_added"]){ ?>
            <!-- timeline item -->
            <div>
            <i class="fas fa-circle bg-dark"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_added"]))?></span>
                <h3 class="timeline-header no-border"><b><?=$data['description']?></b> report was submitted</h3>
            </div>
            </div>
            <!-- END timeline item -->
        <?php }  ?>
            
        <?php if($data["date_approved"]){ ?>
            <!-- timeline item -->
            <div>
            <i class="fas fa-circle bg-dark"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_approved"]))?></span>
                <h3 class="timeline-header no-border"><b><?=$data['description']?></b> report was approved. Officials are now on thier way to take action</h3>
            </div>
            </div>
            <!-- END timeline item -->
        <?php }  ?>

        <?php if($data["date_completed"]){ ?>
            <!-- timeline item -->
            <div>
                <i class="fas fa-circle bg-dark text-sm"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fas fa-calendar-alt mr-1"></i> <?=date("M d, y - H:i",strtotime($data["date_completed"]))?></span>
                    <h3 class="timeline-header no-border"><b><?=$data['description']?></b> is now resolved, Actions have been taken and situation is under control</h3>
                </div>
            </div>
            <!-- END timeline item -->
            <div class="time-label">
                <span class="bg-success">
                    Completed
                </span>
            </div>
        <?php }else{  ?>
            <div class="time-label">
                <span class="bg-warning">
                    In-progress
                </span>
            </div>
        <?php } ?>
      </div>
    </div>
    <!-- /.col -->
  </div>
</div>
<?php }else{
            echo "<h3 class='text-center text-danger'>Cancelled</h3>";
        } 
    } ?>

<div class="col-12">
    <hr>
    <label>Chat:</label>
    <div class="card-footer card-comments">
        <?php 
        $chat_sql = mysqli_query($conn, "SELECT * FROM tbl_chat WHERE type_id = '$typeID' AND type = '$typeC'");
        if(mysqli_num_rows($chat_sql) != 0){
        while($row = mysqli_fetch_array($chat_sql)){
        ?>
        <div class="card-comment">
        <!-- User image -->
        <div class="img-circle img-sm h3">
            <i class="fa fa-user-circle"></i>
        </div>

        <div class="comment-text">
            <span class="username">
                <?=$row['user_id'] == $_SESSION['uid']?"You":getUsername($conn, $row['user_id'],0)?>
                <?php if($row['user_id'] == $_SESSION['uid']){ ?>
                    <span class="text-muted float-right">
                        <button type="button" class="btn btn-tool" onclick="delete_chat(<?=$row['chat_id']?>)">
                        <i class="fas fa-times"></i>
                        </button>
                    </span>
                <?php } ?>
            </span><!-- /.username -->
            <p><?=$row['message']?></p>
        </div>
        <!-- /.comment-text -->
        </div>
        <!-- /.card-comment -->
        <?php } }else{ ?>
        <!-- For no data -->
        <div class="card-comment">
        <!-- User image -->
        <div class="img-circle img-sm h3">
            <i class="fa fa-info-circle"></i>
        </div>

        <div class="comment-text pt-1">
            </span><!-- /.username -->
            No data available.
        </div>
        <!-- /.comment-text -->
        </div>
        <!-- /.card-comment -->
        <?php } ?>
    </div>

    <input type="hidden" id="uID" value="<?=$_SESSION['uid']?>">
    <input type="hidden" id="type" value="<?=$typeC?>">
    <input type="hidden" id="typeID" value="<?=$typeID?>">
</div>