<?php
	if(isset($_POST["rID"]) && isset($_POST["val"])){
		include '../core/config.php';
		$rID = $_POST["rID"];
		$val = $_POST["val"];
		$priority = $_POST["priority"];
		$date = date("Y-m-d h:i:s");

		if($val == 1){
			$update = mysqli_query($conn,"UPDATE tbl_report SET STATUS = '$val', is_read = 0, date_approved = '$date', priority = '$priority' WHERE report_id = '$rID'");
		
			if($update){
				$body = "A report was approved.\nAuthorities are on the way.";
                $title = "A report was approved.\nAuthorities are on the way.";
				$user_id = mysqli_fetch_array(mysqli_query($conn, "SELECT user_id FROM tbl_report WHERE report_id = '$rID'"));
                sendNotif($user_id[0], $title, $body, $conn);
				echo 1;
			}else{
				echo 0;
			}

		}else if($val == 3){

			$update = mysqli_query($conn,"UPDATE tbl_report SET STATUS = '$val', is_read = 0, date_completed = '$date' WHERE report_id = '$rID'");
		
			if($update){
				$body = "Your report is now resolved.\nAuthorities have taken action.";
                $title = "Your report is now resolved.\nAuthorities have taken action.";
				$user_id = mysqli_fetch_array(mysqli_query($conn, "SELECT user_id FROM tbl_report WHERE report_id = '$rID'"));
                sendNotif($user_id[0], $title, $body, $conn);
				echo 1;
			}else{
				echo 0;
			}
			
		}else{

			$reason = "";//$_POST["reason"];
			$update = mysqli_query($conn,"UPDATE tbl_report SET STATUS = '$val' WHERE report_id = '$rID'");
			$add = mysqli_query($conn,"INSERT INTO tbl_cancellation SET REASON = '$reason', report_id = '$rID', type = 0");
		
			if($update && $add){
				echo 1;
			}else{
				echo 0;
			}
		}

	}

?>
