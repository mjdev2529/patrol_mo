<?php
	if(isset($_POST["user_id"])){
        session_start();
		include '../core/config.php';
		$user_id = $_POST["user_id"];
        $role = $_SESSION["role"];

        if($role == 0){
            $sql = mysqli_query($conn,"SELECT * FROM (
            SELECT report_id as notif_id, 'R' as notif_type, '' as appl_type, description as concern, status FROM tbl_report WHERE user_id = '$user_id' AND status !=0 AND is_read = 0
            UNION
            SELECT application_id as notif_id, 'A' as notif_type, application_type as appl_type, '' as concern, status FROM tbl_application WHERE user_id = '$user_id' AND status !=0 AND is_read = 0
            ) as new_tbl");
        }else{
            $sql = mysqli_query($conn,"SELECT report_id as notif_id, 'R' as notif_type, status  FROM tbl_report WHERE priority = 3 AND status = 0 AND is_read = 0");
        }

        if(mysqli_num_rows($sql) == 0){
?>
    
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        No notifications available.
    </a>
<?php
        }else{

        while($row = mysqli_fetch_array($sql)){
            if($row["notif_type"] == "R"){
        $status = $row["status"] == 1?"<b>Approved</b>. Authorities are on thier way":($row["status"] == 2?"<b>Cancelled</b>":($row["status"] == 3?"<b>Resolved</b>":"<b>For Review</b>"));

?>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item" onclick="is_read(<?=$row['notif_id']?>,'R')">
        <p><i class="fas fa-exclamation-circle mr-2"></i> <?=$role==0?"A report with concern: <b>".$row['concern']."</b>, was ".$status.".":"A high priority report was added."?></p>
    </a>
    <?php }else{
        $status_a = $row["status"] == 1?"In-Progress":($row["status"] == 2?"Complete":($row["status"] == 3?"Cancelled":"Applied")); ?>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item" onclick="is_read(<?=$row['notif_id']?>,'A')">
        <p><i class="fas fa-file-alt mr-2"></i> An application for <?=getApplicationType($row['appl_type'])?> was <b><?=$status_a?></b>.</p>
    </a>
<?php }} } }else{ echo false; } ?>