<?php include 'core/config.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>PatrolMo | Log in</title>

	  <!-- Google Font: Source Sans Pro -->
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
	  <!-- icheck bootstrap -->
	  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
	</head>
	<body class="hold-transition login-page" style="height: 60vh !important; background: linear-gradient(90deg, rgb(181, 46, 46), rgb(251 0 0 / 48%)) no-repeat; background-repeat: no-repeat;">
	<div class="login-box">
	  <!-- /.login-logo -->
	  <div class="card">
	    <div class="card-body login-card-body">

		  <div class="login-logo">
			<img src="assets/images/potrolMLogo.png" height="100" width="100"><br>
		    <a href="index.php"><b>Patrol</b>Mo</a><br>
		    <small>Barangay Issue Reporting</small>
		  </div>

	      <form id="formLogin" action="#" method="post">
	        <div class="input-group mb-3">
	          <input type="text" class="form-control" placeholder="Username" name="uname" required="">
	          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-user"></span>
	            </div>
	          </div>
	        </div>
	        <div class="input-group mb-3">
	          <input type="password" class="form-control" placeholder="Password" name="pass" required="">
	          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-lock"></span>
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <!-- /.col -->
	          <div class="col-6 offset-3">
	            <button type="submit" class="btn btn-danger btn-block btn-signin">Sign In</button>
	          </div>
	          <!-- /.col -->
	        </div>
	      </form>
	    </div>
	    <!-- /.login-card-body -->
	  </div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<script src="assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="assets/dist/js/adminlte.min.js"></script>
	</body>
</html>

<script type="text/javascript">
	$("#formLogin").submit( function(e){
		e.preventDefault();
		$(".btn-signin").prop("disabled", true);

		var data = $(this).serialize();
		setTimeout( function(){
			$.ajax({
				type: "POST",
				url: "ajax/auth.php",
				data: data,
				success: function(data){
					if(data == 2){
						alert("Warning: Your account is deactivated. Please contact the administrator.");
						$(".btn-signin").prop("disabled", false);
						$(".btn-signin").html("Sign in");
					}else if(data == 1){
						window.location="main/index.php?page=<?=page_url('dashboard')?>";
					}else{
						alert("Error: Username or password incorrect.");
						$(".btn-signin").prop("disabled", false);
						$(".btn-signin").html("Sign in");
					}
				}
			});
		},2000);
	});
</script>