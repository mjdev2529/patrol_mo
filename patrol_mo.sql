-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2021 at 06:44 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `patrol_mo`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_application`
--

CREATE TABLE `tbl_application` (
  `application_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `application_type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `purpose` mediumtext NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_application`
--

INSERT INTO `tbl_application` (`application_id`, `user_id`, `application_type`, `status`, `age`, `birthdate`, `purpose`, `date_added`) VALUES
(1, 4, 4, 2, 23, '2021-11-29', 'test purpose', '2021-11-03'),
(4, 4, 2, 0, 50, '2021-11-29', 'test purpose', '2021-11-27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cancellation`
--

CREATE TABLE `tbl_cancellation` (
  `rc_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `reason` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cancellation`
--

INSERT INTO `tbl_cancellation` (`rc_id`, `type`, `report_id`, `application_id`, `reason`) VALUES
(1, 0, 1, 0, 'test reason'),
(2, 1, 1, 0, 'test reason'),
(3, 0, 0, 1, 'test reason'),
(4, 0, 0, 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_report`
--

CREATE TABLE `tbl_report` (
  `report_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `description` varchar(200) NOT NULL,
  `attachment` varchar(200) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_report`
--

INSERT INTO `tbl_report` (`report_id`, `user_id`, `address`, `status`, `date_added`, `description`, `attachment`, `priority`) VALUES
(1, 4, 'test address', 1, '2021-11-03', 'test description 1', '../assets/img/Nature vs. Nurture.png-4223957', 1),
(2, 2, 'Purok Isla, Villamonte, Bacolod City', 0, '2021-11-22', 'test description', '', 1),
(4, 15, 'Akishola, Villamonte, Bacolod City', 3, '2021-11-22', 'Rumble', '../assets/img/tumblr_b3336847b4a31df368dd296080d09ae2_228a38de_1280.gif', 3),
(6, 4, 'Hervias, Villamonte, Bacolod City', 1, '2021-11-27', 'Riot', '../assets/img/248415832_2028325970668217_6249660762385615564_n.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(200) NOT NULL,
  `contact_num` varchar(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `name`, `address`, `contact_num`, `username`, `password`, `role`, `status`) VALUES
(1, 'Admin', '', '', 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 1, 0),
(2, 'staff 1', 'test address', '09123456789', 'a', '0cc175b9c0f1b6a831c399e269772661', 1, 1),
(4, 'cardo dalisay', 'test address', '09123456789', 'test', '0cc175b9c0f1b6a831c399e269772661', 0, 0),
(14, 'test name', 'test address', '0912345798', 'testuser', '827ccb0eea8a706c4c34a16891f84e7b', 0, 0),
(15, 'test name', 'test address', '09123456789', 'testuser1', '827ccb0eea8a706c4c34a16891f84e7b', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_application`
--
ALTER TABLE `tbl_application`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `tbl_cancellation`
--
ALTER TABLE `tbl_cancellation`
  ADD PRIMARY KEY (`rc_id`);

--
-- Indexes for table `tbl_report`
--
ALTER TABLE `tbl_report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_application`
--
ALTER TABLE `tbl_application`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_cancellation`
--
ALTER TABLE `tbl_cancellation`
  MODIFY `rc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_report`
--
ALTER TABLE `tbl_report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
