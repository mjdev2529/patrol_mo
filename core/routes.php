<?php
	
	$pages = $_GET["page"];
	if($pages == page_url("dashboard")){
		require '../main/pages/dashboard.php';
	}else if($pages == page_url("sales")){
		require '../main/pages/sales.php';
	}else if($pages == page_url("users")){
		require '../main/pages/users.php';
	}else if($pages == page_url("reports_view")){
		require '../main/pages/reports_view.php';
	}else if($pages == page_url("reports")){
		require '../main/pages/reports.php';
	}else if($pages == page_url("application_details")){
		require '../main/pages/application_details.php';
	}else if($pages == page_url("applications")){
		require '../main/pages/applications.php';
	}else{
		require '../main/pages/404.php';
	}

?>