<?php

function getUserName($id, $post_type)
{
    global $mysqli_connect;
    $getUser = $mysqli_connect->query("SELECT name,count(user_id) as counter FROM tbl_users WHERE user_id='$id'");
    $data = $getUser->fetch_array();
    if ($data['counter'] != 0) {
        $name = $post_type == 1?"Anonymous":$data['name'];
    } else {
        $name = "N/A";
    }


    return  $name;
}

function getUserAddress($id)
{
    global $mysqli_connect;
    $getUser = $mysqli_connect->query("SELECT address,count(user_id) as counter FROM tbl_users WHERE user_id='$id'");
    $data = $getUser->fetch_array();

    $getAddress = $mysqli_connect->query("SELECT address FROM tbl_address WHERE address_id='$data[address]'");
    $dataAddress = $getAddress->fetch_array();
    if ($data['counter'] != 0) {
        $address = $dataAddress['address'];
    } else {
        $address = "N/A";
    }


    return  $address;
}

function getStatus($stat, $type)
{
    if ($type == "R") {
        $status = $stat == 1 ? "Approved" : ($stat == 2 ? "Cancelled" : ($stat == 3 ? "Resolved" : "For Review"));
    } else {
        $status = $stat == 0 ? "Applied" : ($stat == 1 ? "In-Progress" : ($stat == 2 ? "Complete" : "Cancelled"));
    }

    return $status;
}

function getApplicationType($appID){
    if($appID == 1){
        $app_type = "Barangay Certificate";
    }else if($appID == 2){
        $app_type = "Certificate of Indigency";
    }else if($appID == 3){
        $app_type = "Cedula";
    }else if($appID == 4){
        $app_type = "Business Permit";
    }else{
        $app_type = "Barangay Clearance";
    }

    return $app_type;
}

function sendNotif($user_id, $title, $body)
{

    global $mysqli_connect;

    $url = 'https://fcm.googleapis.com/fcm/send';

    // $getToken = $mysqli_connect->query("SELECT api_key FROM tbl_users WHERE user_id = '$user_id'");
    // $idtoken = $getToken->fetch_array();

    $tokens = array($user_id, "");
    // $tokens = array($idtoken[0], "");

    $notification = array('title' => $title, 'text' => $body);

    //This array contains, the token and the notification. The 'to' attribute stores the token.
    $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification, 'priority' => 'high');

    //Generating JSON encoded string form the above array.
    $json = json_encode($arrayToSend);
    //Setup headers:
    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key= dAU7i1ZFS_K5puvgxhmvDZ:APA91bGTgfTl0ZEiyZkutWBSWDck3tRUHG7l9qjqiunKKp-ucMxDP1K2_r5nBCKIEjvpzH7Eu8dh43DKcTVhMaQp6V_35K6hWsk2JvSwLTDGPBIoVAYcCcTzuu3kaId3vCzyzY-ElaMh'; // key here

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //Send the request
    $response = curl_exec($ch);

    //Close request
    curl_close($ch);
    return $response;
}
