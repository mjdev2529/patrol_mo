<?php

	function page_url($page){
		return md5(base64_encode($page));
	}

	function enCrypt($data){
		return base64_encode($data);
	}

	function deCrypt($data){
		return base64_decode($data);
	}

	function getApplicationType($appID){
		if($appID == 1){
			$app_type = "Barangay Certificate";
		}else if($appID == 2){
			$app_type = "Certificate of Indigency";
		}else if($appID == 3){
			$app_type = "Cedula";
		}else if($appID == 4){
			$app_type = "Business Permit";
		}else{
			$app_type = "Barangay Clearance";
		}

		return $app_type;
	}

	function getUsername($conn, $uID, $pType){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT name FROM tbl_users WHERE user_id = '$uID'"));
		$users_name = $pType == 1?"Anonymous":$data[0];
		return $users_name;
	}

	function getUserAddress($conn, $uID){
		$data = mysqli_fetch_array(mysqli_query($conn,"SELECT a.address FROM tbl_users u INNER JOIN tbl_address a ON u.address = a.address_id WHERE u.user_id = '$uID'"));
		$users_address = $data[0]?$data[0].", Villamonte, Bacolod City":"N/A";
		return $users_address;
	}

	function sendNotif($user_id, $title, $body, $conn)
	{

		$url = 'https://fcm.googleapis.com/fcm/send';

		$idtoken = mysqli_fetch_array(mysqli_query($conn, "SELECT api_key FROM tbl_users WHERE user_id = '$user_id'"));
		
		$tokens = array($idtoken[0], "");

		$notification = array('title' => $title, 'text' => $body);

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification, 'priority' => 'high');

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);
		//Setup headers:
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key= AAAAjSk7jyY:APA91bEgtrm5iW8WW1dLRdIBXflonZ5sb91yLHP9ilaURDmZHq7U80kwVts676yuht0_FLBPGdEmGHA5QASeILV6QZ7sqcHCz6v3rRvs-pTEOkhFS4j8fWqz3PL4g_YKhUbMjWvyg4QX'; // key here

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//Send the request
		$response = curl_exec($ch);

		//Close request
		curl_close($ch);
		return $response;
	}

?>