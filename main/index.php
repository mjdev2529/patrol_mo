<?php
  include '../core/config.php';
  session_start();
  $page = isset($_GET['page'])&&$_GET['page']!=""?$_GET['page']:"404";

  $get_notif = mysqli_query($conn,"SELECT notif_id, notif_type FROM (
    SELECT report_id as notif_id, 'R' as notif_type FROM tbl_report WHERE user_id = '$_SESSION[uid]' AND status !=0
    UNION
    SELECT application_id as notif_id, 'A' as notif_type FROM tbl_application WHERE user_id = '$_SESSION[uid]' AND status !=0
    ) as new_tbl");

  $hide = $_SESSION["role"] == 1?"style='display:none;'":"";
  $hide_user = $_SESSION["role"] == 0?"style='display:none;'":"";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PatrolMo</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../assets/dist/css/adminlte.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <!-- jQuery -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../assets/dist/js/adminlte.min.js"></script>
    <!-- ChartJS -->
    <script src="../assets/plugins/chart.js/Chart.min.js"></script>
    <!-- DataTables -->
    <script src="../assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- AdminLTE for demo purposes -->
    <!-- <script src="../assets/dist/js/demo.js"></script> -->

  </head>
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-danger navbar-danger">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown" <?=$hide?>>
          <a class="nav-link text-light" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge badge-user" style="display:none;">
              0
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right notif" style="left: inherit; right: 0px;">
            <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                  No notifications available.
              </a>
          </div>
        </li>
        <li class="nav-item dropdown" <?=$hide_user?>>
          <a class="nav-link text-light" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-warning badge-admin" style="display:none;">
              0
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right notif" style="left: inherit; right: 0px;">
            
          </div>
        </li>
          <li class="nav-item">
            <a href="#" class="nav-link btn btn-outline-light" role="button" onclick="logout()">
              Logout <i class="fa fa-sign-out-alt ml-1"></i>
            </a>
          </li>
        </ul>
      </nav>

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-danger elevation-4">
        <!-- Brand Logo -->
        <a href="index.php?page=<?=page_url('dashboard')?>" class="brand-link bg-danger">
          <img src="../assets/images/potrolMLogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-bolder">PatrolMo</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user (optional) -->
          <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="col-12 text-center">
              <?php if($_SESSION['role'] == 1){?>
                <a href="#">Administrator</a>
              <?php }else{ ?>
                <a href="#"><?php //getUserBranch($conn, $_SESSION["uid"]);?></a>
              <?php } ?>
            </div>
          </div> -->

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <?php include 'components/sidenav.php'; ?>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php include '../core/routes.php'; ?>
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <script type="text/javascript">
      $(document).ready( function(){
        countUpdate();
        getUpdate();
      });

      //GLOBAL SCRIPT
      function logout(){
        var x = confirm("Are you sure to end your session?");
        if(x){
          window.location.href="../ajax/logout.php";
        }
      }

      <?php if($_SESSION["role"] == 0){?>
        function countUpdate(){
          $.ajax({
            type: "POST",
            url: "../ajax/updates_count.php",
            data: {user_id: "<?=$_SESSION['uid']?>"},
            success: function(data){
              if(data != 0){
                $(".badge-user").show();
                $(".badge-user").html(data);
                // $(".notif").show();
              }else{
                $(".badge-user").hide();
                // $(".notif").hide();
              }
            }
          });
        }

        function getUpdate(){
          $.ajax({
            type: "POST",
            url: "../ajax/updates_get.php",
            data: {user_id: "<?=$_SESSION['uid']?>"},
            success: function(data){
              if(data != null){
                $(".notif").html(data);
              }
            }
          });
        }

        

        function is_read(nID, type){
          $.ajax({
            type: "POST",
            url: "../ajax/update_notif.php",
            data: {nID: nID, type: type},
            success: function(data){
              if(data == 1){
                if(type == "A"){
                  window.location.href="index.php?page=<?=page_url('applications')?>";
                  countUpdate();
                  getUpdate();
                }else{
                  window.location.href="index.php?page=<?=page_url('reports')?>";
                  countUpdate();
                  getUpdate();

                }
              }
            }
          });
        }

        // var test = 1;
        setInterval(() => {
          countUpdate();
          getUpdate();
        }, 15000);
      <?php }else{ ?>
        function countUpdate(){
          $.ajax({
            type: "POST",
            url: "../ajax/updates_count.php",
            data: {user_id: "<?=$_SESSION['uid']?>"},
            success: function(data){
              if(data != 0){
                $(".badge-admin").show();
                $(".badge-admin").html(data);
                // $(".notif").show();
              }else{
                $(".badge-admin").hide();
                // $(".notif").hide();
              }
            }
          });
        }

        function getUpdate(){
          $.ajax({
            type: "POST",
            url: "../ajax/updates_get.php",
            data: {user_id: "<?=$_SESSION['uid']?>"},
            success: function(data){
              if(data != null){
                $(".notif").html(data);
              }
            }
          });
        }

        

        function is_read(nID, type){
          if(type == "A"){
              window.location.href="index.php?page=<?=page_url('applications')?>";
            }else{
              window.location.href="index.php?page=<?=page_url('reports')?>";
            }
        }

        // var test = 1;
        setInterval(() => {
          countUpdate();
          getUpdate();
        }, 15000);
        <?php } ?>

    </script>
  </body>
</html>
