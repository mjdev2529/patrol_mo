<div class="main">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><a href="index.php?page=<?=page_url('dashboard')?>">Dashboard</a> / <span class="text-muted">Sales</span></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <div class="h5 mr-5">
        <i class="fa fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
      </div>
      <div class="h5">
        <i class="fa fa-calendar mr-1"></i> <?=date("F d, Y");?>
      </div>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-12">
      <button type="button" class="btn btn-outline-primary mb-3"><i class="fa fa-arrow-left"></i> Back</button>
    </div>
    

</div>

<!-- PAGE SCRIPT -->
<script type="text/javascript">
</script>