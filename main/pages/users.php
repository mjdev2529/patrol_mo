<?php
  $hide = $_SESSION["role"] == 1?"style='display:none;'":"";
  $hide_user = $_SESSION["role"] == 0?"style='display:none;'":"";
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1>User Management</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card" <?=$hide_user?>>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Users List</h5>
                  <div class="card-tools" <?=$hide_user?>>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_user_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_users()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tbl_users" class="table table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAll" onclick="checkAll()" <?=$hide_user?>></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th width="150px">Role</th>
                        <th width="100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

      <!-- Default box -->
      <div class="card" <?=$hide?>>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card col-8 offset-2">
                <div class="card-header">
                  <h5 class="card-title">User Profile</h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <form id="edit_user_form1" method="POST" action="#">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Name</label>
                        <input type="text" name="pname" id="pname1" class="form-control" placeholder="Name" required="">
                        <input type="hidden" name="uID" id="uID1">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Address</label>
                        <select class="form-control" name="address" id="address1">
                          <option value="0">Select:</option>
                          <?php
                            $get_address = mysqli_query($conn, "SELECT * FROM tbl_address ORDER BY address");
                            while($row = mysqli_fetch_array($get_address)){
                          ?>
                            <option value="<?=$row['address_id']?>"><?=$row['address']?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Contact No.</label>
                        <input type="text" name="cnum" id="cnum1" class="form-control" placeholder="Contact No." required="" maxlength="11">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Email Address</label>
                        <input type="text" name="email" id="email1" class="form-control" placeholder="Email Address" required="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Username</label>
                        <input type="text" name="uname" id="uname1" class="form-control" placeholder="Username" required="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="pass" class="form-control" placeholder="Password">
                        <input type="hidden" name="role" id="role1">
                      </div>
                    </div>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>

                  </form>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- ADD MD -->
    <div class="modal fade" id="add_user_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add new user</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_user_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Name</label>
                  <input type="text" name="pname" class="form-control" placeholder="Name" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Address</label>
                  <select class="form-control" name="address" id="address1">
                    <option value="0">Select:</option>
                    <?php
                      $get_address = mysqli_query($conn, "SELECT * FROM tbl_address ORDER BY address");
                      while($row = mysqli_fetch_array($get_address)){
                    ?>
                      <option value="<?=$row['address_id']?>"><?=$row['address']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Contact No.</label>
                  <input type="text" name="cnum" class="form-control" placeholder="Contact No." required="" maxlength="11">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Email Address</label>
                  <input type="text" name="email" class="form-control" placeholder="Email Address" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="uname" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="pass" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Role</label>
                  <select class="form-control" name="role">
                    <option value="-1">Select Role:</option>
                    <option value="0">Citizen</option>
                    <option value="1">Admin</option>
                  </select>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- EDIT MD -->
    <div class="modal fade" id="edit_user_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add new user</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_user_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Name</label>
                  <input type="text" name="pname" id="pname" class="form-control" placeholder="Name" required="">
                  <input type="hidden" name="uID" id="uID">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Address</label>
                  <textarea name="address" id="address" class="form-control" placeholder="Type Here..." required=""></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Contact No.</label>
                  <input type="text" name="cnum" id="cnum" class="form-control" placeholder="Contact No." required="" maxlength="11">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Email Address</label>
                  <input type="text" name="email" id="email" class="form-control" placeholder="Email Address" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="uname" id="uname" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="pass" class="form-control" placeholder="Password">
                </div>
                <div class="form-group" <?=$hide?>>
                  <label for="exampleInputPassword1">Role</label>
                  <select class="form-control" name="role" id="role">
                    <option value="-1">Select Role:</option>
                    <option value="1">Admin</option>
                  </select>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_users();
        <?php if($_SESSION['role'] == 0){?>
          user_details1(<?=$_SESSION['uid']?>)
        <?php }?>
      });

      function get_users(){
        $("#tbl_users").DataTable().destroy();
        $("#tbl_users").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/users_data.php",
          },
          "processing": true,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.user_id+"' name='cb_users' <?=$hide_user?>>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "name"
          },
          {
            "data": "role"
          },
          {
            "mRender": function(data, type, row){
              <?php
                if($_SESSION["role"] == 0){
              ?>
                return "<button class='btn btn-sm btn-outline-dark' onclick='user_details("+row.user_id+")' <?=$hide?>>Edit User</button>";
              <?php }else{ ?>
                return "<button class='btn btn-sm btn-outline-dark' onclick='user_status("+row.user_id+")' "+row.hide2+">Deactivate</button>"+
                "<button class='btn btn-sm btn-outline-dark' onclick='user_status("+row.user_id+")' "+row.hide+">Activate</button>";
              <?php } ?>
            }
          }
          ]
        });
      }

      function checkAll(){
        var cbAll = $("#checkAll").is(":checked");
        if(cbAll){
          $("input[name=cb_users]").prop("checked", true);
        }else{
          $("input[name=cb_users]").prop("checked", false);
        }
      }

      $("#add_user_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_add.php";
        if($("select[name=role]").val()*1 >= 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data){
              if(data == 1){
                alert("Success: New user was added.");
                $("input").val("");
                $("textarea").val("");
                $("select").val("-1");
                $("#add_user_md").modal("hide");
                get_users();
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }else{
          alert("Notice: Please input all required fields.");
        }
      });

      function delete_users(){
        var url = "../ajax/user_delete.php";
        var conf = confirm("Are you sure to delete user/s?");
        var uID = [];
        $("input[name=cb_users]:checked").each( function(){
          uID.push($(this).val());
        });

        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {uID: uID},
            success: function(data){
              if(data != 0){
                alert("Success: Selected user/s was removed.");
                get_users();
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }

      }

      function user_status(uID){
        var url = "../ajax/user_status.php";
        var conf = confirm("Are you sure to change user status?");

        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {uID: uID},
            success: function(data){
              if(data != 0){
                alert("Success: User's status was changed.");
                get_users();
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }

      }

      function user_details(uID){
        var url = "../ajax/user_details.php";
        $.ajax({
            type: "POST",
            url: url,
            data: {uID: uID},
            success: function(data){
              if(data != ""){
                var o = JSON.parse(data);
                $("#uID").val(uID);
                $("#pname").val(o.name);
                $("#address").val(o.address);
                $("#cnum").val(o.contact_num);
                $("#uname").val(o.username);
                $("#role").val(o.role);
                $("#email").val(o.email);

                $("#edit_user_md").modal();
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
      }

      $("#edit_user_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_update.php";
        if($("#role").val()*1 >= 0){
          $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data){
              if(data == 1){
                alert("Success: User data was updated.");
                $("input").val("");
                $("textarea").val("");
                $("select").val("-1");
                $("#edit_user_md").modal("hide");
                get_users();
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }else{
          alert("Notice: Please input all required fields.");
        }
      });

      function user_details1(uID){
        var url = "../ajax/user_details.php";
        $.ajax({
            type: "POST",
            url: url,
            data: {uID: uID},
            success: function(data){
              if(data != ""){
                var o = JSON.parse(data);
                $("#uID1").val(uID);
                $("#pname1").val(o.name);
                $("#address1").val(o.address);
                $("#cnum1").val(o.contact_num);
                $("#uname1").val(o.username);
                $("#role1").val(o.role);
                $("#email1").val(o.email);
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
      }

      $("#edit_user_form1").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/user_update.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: User data was updated.");
              window.location.reload();
            }else{
              alert("Error: Something was wrong.");
            }
          }
        });
      });

    </script>