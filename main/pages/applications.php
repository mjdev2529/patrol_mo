<?php
  $hide = $_SESSION["role"] == 1?"style='display:none;'":"";
  $hide_user = $_SESSION["role"] == 0?"style='display:none;'":"";
  $row = mysqli_fetch_array(mysqli_query($conn," SELECT * FROM tbl_users WHERE user_id = '$_SESSION[uid]'"));
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1>Applications</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Applications List</h5>
                  <div class="card-tools" <?=$hide?>>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_application_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_application()">
                      Delete
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <div class="card-tools mb-3" <?=$hide_user?>>
                    <div class="row">
                      <span class="col-1 offset-10"><b>Filter:</b></span>
                      <select class="form-control col-1" id="app_filter" onchange="application_filter()">
                        <option value="1">All</option>
                        <option value="2">Applied</option>
                        <option value="3">In-progress</option>
                        <option value="4">Complete</option>
                        <option value="5">Cancelled</option>
                      </select>
                    </div>
                  </div>
                  <table id="tbl_applications" class="table table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAll" onclick="checkAll()" <?=$hide?>></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th width="150px">Application Type</th>
                        <th width="150px">Status</th>
                        <th width="150px">Date Added</th>
                        <th width="100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- ADD MD -->
    <div class="modal fade" id="add_application_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Application details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_application_form" method="POST" action="#" enctype="multipart/form-data">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Name</label>
                  <input type="text" class="form-control" placeholder="Name" value="<?=$_SESSION['name']?>" readonly>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Address</label>
                  <textarea class="form-control" readonly placeholder="Type Here..."><?=getUserAddress($conn, $row["user_id"])?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Age</label>
                  <input type="text" class="form-control clear" placeholder="Age" maxlength="2" name="age" id="age">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Birthdate</label>
                  <input type="date" class="form-control clear" placeholder="Birthdate" name="bdate" id="bdate">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Purpose</label>
                  <textarea class="form-control clear" placeholder="Type Here..." name="purpose" id="purpose"></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Application for</label>
                  <select class="form-control" name="app_type" id="app_type">
                    <option value="0">Select:</option>
                    <option value="1">Barangay Certificate</option>
                    <option value="2">Certificate of Indigency</option>
                    <option value="3">Cedula</option>
                    <option value="4">Business Permit</option>
                    <option value="5">Barangay Clearance</option>
                  </select>
                  <input type="hidden" id="a_id" name="a_id" value="0">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- VIEW MD -->
    <div class="modal fade" id="view_status_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Application status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body status-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_applications(1);
        $("#add_application_md").on("hidden.bs.modal", function(){
          $(".clear").val("");
          $("select").val("0");
        });
      });

      function get_applications(filter){
        $("#tbl_applications").DataTable().destroy();
        $("#tbl_applications").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/application_data.php",
            "data": {filter: filter}
          },
          "processing": true,
          "columns": [  
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.application_id+"' name='cb_apps' <?=$hide?>>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "user"
          },
          {
            "data": "application_type"
          },
          {
            "mRender": function(data, type, row){
              <?php if($_SESSION["role"] == 0){ ?>
                return "<a href='#' onclick='view_status(0,"+row.application_id+")'><u>View</u></a>";
              <?php }else{ ?>
                return row.status;
              <?php } ?>
            }
          },
          {
            "data": "date_added"
          },
          {
            "mRender": function(data, type, row){
              var hide = row.stat == 2?"disabled":"";
              return "<button class='btn btn-sm btn-outline-dark' onclick='view_details("+row.application_id+")' <?=$hide_user?>>View Details</button>"+
              "<button class='btn btn-sm btn-outline-dark' onclick='edit_application("+row.application_id+")' <?=$hide?> "+hide+">Edit Application</button>";
            }
          }
          ]
        });
      }

      function view_details(appID){
        window.location.href="index.php?page=<?=page_url('application_details')?>&app_id="+appID;
      }

      function checkAll(){
        var cb = $("#checkAll").is(":checked");
        if(cb){
          $("input[name=cb_apps]").prop("checked", true);
        }else{
          $("input[name=cb_apps]").prop("checked", false);
        }
      }

      $("#add_application_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/application_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 2){
              alert("Success: Application details was updated.");
              $("select").val("0");
              $("#add_application_md").modal("hide");
              get_applications(1);
            }else if(data == 1){
              alert("Success: New application was added.");
              $("select").val("0");
              $("#add_application_md").modal("hide");
              $(".clear").val("");
              get_applications(1);
            }else{
              alert("Error: Something was wrong.");
            }
          }
        });
      });

      function edit_application(aID){
        var url = "../ajax/application_details.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {aID: aID},
          success: function(data){
            if(data){
              $("#add_application_md").modal();
              var o = JSON.parse(data);
              $("#a_id").val(aID);
              $("#app_type").val(o.application_type);
              $("#age").val(o.age);
              $("#bdate").val(o.birthdate);
              $("#purpose").val(o.purpose);
            }
          }
        });
      }

      function delete_application(){
        var url = "../ajax/application_delete.php";
        var conf = confirm("Are you sure to delete application/s?");
        var aID = [];
        $("input[name=cb_apps]:checked").each( function(){
          aID.push($(this).val());
        });

        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {aID: aID},
            success: function(data){
              if(data != 0){
                alert("Success: Selected application/s was removed.");
                get_applications(1);
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }

      }
      
      function application_filter(filter){
        var filter = $("#app_filter").val();
        get_applications(filter);
      }

      function view_status(type, app_id){
        var url = "../ajax/view_status.php";
        $.ajax({
            type: "POST",
            url: url,
            data: {app_id: app_id, type: type},
            success: function(data){
              if(data){
                  $("#view_status_md").modal();
                  $(".status-body").html(data);
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
      }
    </script>