<?php
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_report WHERE report_id = '$_GET[r_id]'"));
  $status = $row["status"] == 1?"<span class='text-primary'>Confirmed</span>":($row["status"] == 2?"<span class='text-danger'>Cancelled</span>":($row["status"] == 3?"<span class='text-success'>Resolved</span>":"<span class='text-dark'>For Review</span>"));
  // $address = mysqli_fetch_array(mysqli_query($conn, "SELECT address FROM tbl_users WHERE user_id = '$row[user_id]'"));
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1><a href="index.php?page=<?=page_url('reports')?>"><i class="fa fa-chevron-left"></i> Incident Report</a> / View</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-8 offset-2">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Incident Report Details</b></h5>
                  <div class="card-tools">
                    <?php
                      if($row['status'] == 0){
                    ?>
                      <button type="button" class="btn btn-sm btn-success" onclick="respond_report(1)">
                        Respond
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="cancel_report(2)">
                        Cancel
                      </button>
                    <?php }else if($row["status"] == 1){ ?>
                      <button type="button" class="btn btn-sm btn-success" onclick="respond_report(3)" <?=$row["status"]==3?"disabled":""?>>
                        Resolved
                      </button>
                    <?php }else{ echo ""; } ?>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="col-10 offset-1">
                    <div class="row">
                      <div class="col-md-3"><label>Name:</label></div>
                      <div class="col-md-3"><?=strtoupper(getUsername($conn, $row["user_id"], 0))?></div>

                      <div class="col-md-3"><label>Date Applied:</label></div>
                      <div class="col-md-3"><?=date("Y-m-d", strtotime($row["date_added"]))?></div>

                      <div class="col-md-3"><label>Address: </label></div>
                      <div class="col-md-3"><u><a href="http://maps.google.com/?q=<?=$row['address']?>" target="_blank"><?=$row["address"]?></a></u></div>
                      <div class="col-md-3"><label>Status:</label> </div>
                      <div class="col-md-3"><?=$status?></div>

                      <div class="col-md-3"><label>Concern: </label></div>
                      <div class="col-md-9"><?=$row["description"]?></div>

                      <div class="col-md-3"><label>Priority: </label></div>
                        <?php
                          if($row["priority"] != 0){
                        ?>
                          <div class="col-md-3">
                            <?=$row["priority"] == 1?"<span class='text-dark'>Low</span>":($row["priority"] == 2?"<span class='text-warning'>Medium</span>":($row["priority"] == 3?"<span class='text-danger'>High</span>":"For Review"));?>
                          </div>
                        <?php }else{ ?>
                          <div class="col-md-12">
                            <div class="form-group">
                              <!-- <label for="exampleInputPassword1">Priority</label><br> -->
                              <label><input type="checkbox" name="prio" id="prio1" value="1" class="mr-2">Low</label> - For broken post, garbage issues and the likes. <br>
                              <label><input type="checkbox" name="prio" id="prio2" value="2" class="mr-2">Medium</label> - Clogged canals, reeking pig pen and the likes. <br>
                              <label><input type="checkbox" name="prio" id="prio3" value="3" class="mr-2">High</label> - For Burning house, riot and the likes. 
                            </div>
                          </div>
                        <?php } ?>
                      <div class="col-12">
                        <hr>
                        <label>Attachment:</label>
                        <div class="row mt-1">
                          <?php
                            $getImgs = mysqli_query($conn, "SELECT * FROM tbl_attachment WHERE report_id = '$row[report_id]'");
                            if(mysqli_num_rows($getImgs) != 0){
                              while($imgData = mysqli_fetch_array($getImgs)){
                          ?>
                            <div class="col-4">
                              <img src="<?=$imgData['slug']?>" width="100%">
                            </div>
                          <?php } }else { echo "<div class='col-12'><h1 class='text-center'>No Data Available.</h1></div>"; } ?>
                        </div>
                      </div>
                      <div class="col-12">
                        <hr>
                        <label>Chat:</label>
                        <div class="card-footer card-comments">
                          <?php 
                            $chat_sql = mysqli_query($conn, "SELECT * FROM tbl_chat WHERE type_id = '$_GET[r_id]' AND type = 'R'");
                            if(mysqli_num_rows($chat_sql) != 0){
                            while($row1 = mysqli_fetch_array($chat_sql)){
                          ?>
                          <div class="card-comment">
                            <!-- User image -->
                            <div class="img-circle img-sm h3">
                              <i class="fa fa-user-circle"></i>
                            </div>

                            <div class="comment-text">
                              <span class="username">
                                <?=$_SESSION['role'] == 1?"You":getUsername($conn, $_SESSION['uid'],0)?>
                                <span class="text-muted float-right">
                                  <button type="button" class="btn btn-tool" onclick="delete_chat(<?=$row1['chat_id']?>)">
                                    <i class="fas fa-times"></i>
                                  </button>
                              </span>
                              </span><!-- /.username -->
                              <p><?=$row1['message']?></p>
                            </div>
                            <!-- /.comment-text -->
                          </div>
                          <!-- /.card-comment -->
                          <?php } }else{ ?>
                          <!-- For no data -->
                          <div class="card-comment">
                            <!-- User image -->
                            <div class="img-circle img-sm h3">
                              <i class="fa fa-info-circle"></i>
                            </div>

                            <div class="comment-text pt-1">
                              </span><!-- /.username -->
                              No data available.
                            </div>
                            <!-- /.comment-text -->
                          </div>
                          <!-- /.card-comment -->
                          <?php } ?>
                        </div>

                        <div class="card-footer">
                          <form id="chat_form" action="#" method="post" autocomplete="off">
                            <div class="input-group">
                              <input type="text" name="message" placeholder="Type Message ..." class="form-control" required autofocus>
                              <input type="hidden" name="uID" value="<?=$_SESSION['uid']?>">
                              <input type="hidden" name="type" value="R">
                              <input type="hidden" name="typeID" value="<?=$_GET['r_id']?>">
                              <span class="input-group-append">
                                <button type="submit" class="btn btn-outline-primary"><i class="far fa-paper-plane"></i></button>
                              </span>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <div class="modal fade" id="cancel_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reason for cancellation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="cancel_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Reason</label>
                  <textarea name="reason" class="form-control" placeholder="Type Here..."></textarea>
                  <input type="hidden" name="val" value="2">
                  <input type="hidden" name="rID" value="<?=$_GET['r_id']?>">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">

      if('<?=$row["status"]?>' == "3"){
        $("input[type=text]").prop("disabled", true);
        $("button").prop("disabled", true);
      }

      $("#chat_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/chat_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_chat(cID){

        var url = "../ajax/chat_delete.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }

      function respond_report(val){
        var rID = "<?=$_GET['r_id']?>";
        var checker = "<?=$row["priority"]?>" == "0"?$("input[name=prio]").is(":checked"):true;
        if(checker){
          var conf = confirm("Are you sure to respond on this report?");
          var priority = $("input[name=prio]:checked").val();
          if(conf){
            var url = "../ajax/report_respond.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {rID: rID, val: val, priority: priority},
              success: function(data){
                if(data == 1){
                  alert("Success: Report sender has been notified!");
                  window.location.reload();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }
        }else{
          alert("Please select priority!");
        }
      }

      function cancel_report(val){
        var conf = confirm("Are you sure to cancel this report?");
        if(conf){
          //$("#cancel_md").modal();
          cancelReport();
        }
      }

      ///$("#cancel_form").submit( 
        function cancelReport(){
        //e.preventDefault();
        //var data = $(this).serialize();
                var rID = "<?=$_GET['r_id']?>";

        var url = "../ajax/report_respond.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {rID: rID, val: 2},
          success: function(data){
            if(data == 1){
              alert("Success: Report has been cancelled.");
              window.location.reload();
              //$("#cancel_md").modal("hide");
              //$("textarea").val("");
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }
      //);

      $("#prio1").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio1").prop("checked", true);
      });

      $("#prio2").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio2").prop("checked", true);
      });

      $("#prio3").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio3").prop("checked", true);
      });

    </script>
