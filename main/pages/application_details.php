<?php
  $row = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM tbl_application WHERE application_id = '$_GET[app_id]'"));
  $app_type = getApplicationType($row["application_type"]);
  $status = $row["status"] == 0?"<span class='text-dark'>Applied</span>":($row["status"] == 1?"<span class='text-warning'>In-Progress</span>":($row["status"] == 3?"<span class='text-danger'>Cancelled</span>":"<span class='text-success'>Complete</span>"));
  $data = mysqli_fetch_array(mysqli_query($conn, "SELECT address, contact_num FROM tbl_users WHERE user_id = '$row[user_id]'"));
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1><a href="index.php?page=<?=page_url('applications')?>"><i class="fa fa-chevron-left"></i> Applications</a> / Details</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-8 offset-2">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title"><?=$app_type?> Application for <b><?=strtoupper(getUsername($conn, $row["user_id"], 0))?></b></h5>
                  <div class="card-tools">
                    <?php if($row["status"] == 1){?>
                      <button type="button" class="btn btn-sm btn-success" onclick="application_complete(2)">
                        Complete
                      </button>
                    <?php }else if($row["status"] == 0){ ?>
                      <button type="button" class="btn btn-sm btn-outline-success" onclick="application_approve(1)">
                        Approve
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="application_cancel(3)">
                        Cancel
                      </button>
                    <?php }else{ echo "";} ?>

                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="col-10 offset-1">
                    <div class="row">
                      <div class="col-md-3"><label>Name:</label></div>
                      <div class="col-md-3"><?=strtoupper(getUsername($conn, $row["user_id"], 0))?></div>

                      <div class="col-md-3"><label>Date Applied:</label></div>
                      <div class="col-md-3"><?=date("Y-m-d", strtotime($row["date_added"]))?></div>

                      <div class="col-md-3"><label>Address: </label></div>
                      <div class="col-md-3"><?=getUserAddress($conn,$data[0])?></div>
                      <div class="col-md-3"><label>Status: </label></div>
                      <div class="col-md-3"><?=$status?></div>

                      <div class="col-md-3"><label>Contact No.: </label></div>
                      <div class="col-md-3"><?=$data[1]?></div>
                    </div>
                  </div>
                  
                  <div class="col-12">
                    <hr>
                    <label>Chat:</label>
                    <div class="card-footer card-comments">
                      <?php 
                        $chat_sql = mysqli_query($conn, "SELECT * FROM tbl_chat WHERE type_id = '$_GET[app_id]' AND type = 'A'");
                        if(mysqli_num_rows($chat_sql) != 0){
                        while($row1 = mysqli_fetch_array($chat_sql)){
                      ?>
                      <div class="card-comment">
                        <!-- User image -->
                        <div class="img-circle img-sm h3">
                          <i class="fa fa-user-circle"></i>
                        </div>

                        <div class="comment-text">
                          <span class="username">
                            <?=$_SESSION['role'] == 1?"You":getUsername($conn, $_SESSION['uid'],0)?>
                            <span class="text-muted float-right">
                              <button type="button" class="btn btn-tool" onclick="delete_chat(<?=$row1['chat_id']?>)">
                                <i class="fas fa-times"></i>
                              </button>
                          </span>
                          </span><!-- /.username -->
                          <p><?=$row1['message']?></p>
                        </div>
                        <!-- /.comment-text -->
                      </div>
                      <!-- /.card-comment -->
                      <?php } }else{ ?>
                      <!-- For no data -->
                      <div class="card-comment">
                        <!-- User image -->
                        <div class="img-circle img-sm h3">
                          <i class="fa fa-info-circle"></i>
                        </div>

                        <div class="comment-text pt-1">
                          </span><!-- /.username -->
                          No data available.
                        </div>
                        <!-- /.comment-text -->
                      </div>
                      <!-- /.card-comment -->
                      <?php } ?>
                    </div>

                    <div class="card-footer">
                      <form id="chat_form" action="#" method="post" autocomplete="off">
                        <div class="input-group">
                          <input type="text" name="message" placeholder="Type Message ..." class="form-control" required autofocus>
                          <input type="hidden" name="uID" value="<?=$_SESSION['uid']?>">
                          <input type="hidden" name="type" value="A">
                          <input type="hidden" name="typeID" value="<?=$_GET['app_id']?>">
                          <span class="input-group-append">
                            <button type="submit" class="btn btn-outline-primary"><i class="far fa-paper-plane"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <div class="modal fade" id="cancel_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reason for cancellation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="cancel_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Reason</label>
                  <textarea name="reason" class="form-control" placeholder="Type Here..."></textarea>
                  <input type="hidden" name="val" value="2">
                  <input type="hidden" name="aID" value="<?=$_GET['app_id']?>">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      if('<?=$row["status"]?>' == "2"){
        $("input").prop("disabled", true);
        $("button").prop("disabled", true);
      }

      $("#chat_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/chat_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_chat(cID){

        var url = "../ajax/chat_delete.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }

      function application_approve(val){
        var app_id = "<?=$_GET['app_id']?>";
        var conf = confirm("Are you sure to approve this application?");
        if(conf){
          var url = "../ajax/app_approve.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {app_id: app_id, val: val},
            success: function(data){
              if(data == 1){
                alert("Success: Application was approved!");
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function application_complete(val){
        var app_id = "<?=$_GET['app_id']?>";
        var conf = confirm("Are you sure to complete this application?");
        if(conf){
          var url = "../ajax/app_approve.php";
          $.ajax({
            type: "POST",
            url: url,
            data: {app_id: app_id, val: val},
            success: function(data){
              if(data == 1){
                alert("Success: Application was complete!");
                window.location.reload();
              }else{
                alert("Error: Something is wrong.");
              }
            }
          });
        }
      }

      function application_cancel(val){
        var conf = confirm("Are you sure to cancel this report?");
        if(conf){
          appCancel(val);//$("#cancel_md").modal();
        }
      }

      //$("#cancel_form").submit( 
        function appCancel(val){
        //e.preventDefault();
        //var data = $(this).serialize();
                var app_id = "<?=$_GET['app_id']?>";

        var url = "../ajax/app_cancel.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {app_id: app_id, val: val},
          success: function(data){
            if(data == 1){
              alert("Success: Application was declined.");
              window.location.reload();
              //$("#cancel_md").modal("hide");
             // $("textarea").val("");
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }
      //);
    </script>
