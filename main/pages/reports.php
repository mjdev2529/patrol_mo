<?php
  $hide = $_SESSION["role"] == 1?"style='display:none;'":"";
  $hide_user = $_SESSION["role"] == 0?"style='display:none;'":"";

  $total_yr = mysqli_num_rows(mysqli_query($conn,"SELECT report_id FROM tbl_report WHERE YEAR(date_added) = '".date("Y")."'"));
  $total_mo = mysqli_num_rows(mysqli_query($conn,"SELECT report_id FROM tbl_report WHERE MONTH(date_added) = '".date("m")."'"));
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1>Incident Reports</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

        <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title">Reports List</h5>
                  <div class="card-tools" <?=$hide?>>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_report_md">
                      Add
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_report()">
                      Delete
                    </button>
                  </div>

                </div>
                <!-- /.card-header -->
                <div class="card-body">

                  <div class="card-tools mb-3" <?=$hide_user?>>
                    <div class="row">
                      <span class="col-1 offset-10"><b>Filter:</b></span>
                      <select class="form-control col-1" id="rp_filter" onchange="report_filter()">
                        <option value="1">All</option>
                        <option value="2">For review</option>
                        <option value="3">Approved</option>
                        <option value="4">Resolved</option>
                        <option value="5">Cancelled</option>
                      </select>
                    </div>
                  </div>
                  <table id="tbl_reports" class="table table-condensed table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px"><input type="checkbox" id="checkAll" onclick="checkAll()" <?=$hide?>></th>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Concerns</th>
                        <th width="100px">Status</th>
                        <th width="100px">Priority</th>
                        <th width="100px">Date Added</th>
                        <th width="100px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- ADD MD -->
    <div class="modal fade" id="add_report_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add new report</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_report_form" method="POST" action="#" enctype="multipart/form-data">
              <div class="card-body">
              <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea name="description" class="form-control" placeholder="Type Here..." required=""></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Address</label>
                  <textarea name="address" class="form-control" placeholder="Type Here..." required="" readonly=""><?=getUserAddress($conn, $_SESSION['uid'])?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Attachment</label>
                  <input type="file" name="attachment[]" class="form-control" required="" accept="image/png, image/gif, image/jpeg" multiple="multiple">
                </div>
                <!-- <div class="form-group">
                  <label for="exampleInputPassword1">Priority</label><br>
                  <label><input type="checkbox" name="prio" value="1" class="mr-2 cb1" onclick="cb1()">Low</label> - For broken post, garbage issues and the likes. <br>
                  <label><input type="checkbox" name="prio" value="2" class="mr-2 cb2" onclick="cb2()">Medium</label> - Clogged canals, reeking pig pen and the likes. <br>
                  <label><input type="checkbox" name="prio" value="3" class="mr-2 cb3" onclick="cb3()">High</label> - For Burning house, riot and the likes. 
                </div> -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- EDIT MD -->
    <div class="modal fade" id="edit_report_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit report details</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_report_form" method="POST" action="#" enctype="multipart/form-data">
              <div class="card-body">
              <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea name="description" id="description" class="form-control" placeholder="Type Here..." required=""></textarea>
                  <input type="hidden" name="r_id" id="r_id">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Address</label>
                  <textarea name="address" id="address" class="form-control" placeholder="Type Here..." required="" readonly=""><?=getUserAddress($conn, $_SESSION[uid])?></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Attachment</label>
                  <input type="file" name="attachment[]" class="form-control" accept="image/png, image/gif, image/jpeg" multiple="multiple">
                </div>

                <div class="form-group">
                <label for="exampleInputPassword1">Attachment Preview</label>
                  <div class="col-12 img-container">
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label for="exampleInputPassword1">Priority</label><br>
                  <label><input type="checkbox" name="prio" id="prio1" value="1" class="mr-2">Low</label> - For broken post, garbage issues and the likes. <br>
                  <label><input type="checkbox" name="prio" id="prio2" value="2" class="mr-2">Medium</label> - Clogged canals, reeking pig pen and the likes. <br>
                  <label><input type="checkbox" name="prio" id="prio3" value="3" class="mr-2">High</label> - For Burning house, riot and the likes. 
                </div> -->
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- REPORT MD -->
    <div class="modal fade" id="view_report_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Statistical report</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row" <?=$hide?>>
              <div class="col-12">
                <!-- small box -->
                <div class="small-box bg-light">
                  <div class="inner">
                    <h3><?=$total_yr!=0?$total_yr:0?></h3>

                    <p>Yearly Incident Reports</p>
                  </div>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-12">
                <!-- small box -->
                <div class="small-box bg-light">
                  <div class="inner">
                    <h3><?=$total_mo!=0?$total_mo:0?></h3>

                    <p>Monthly Incident Reports</p>
                  </div>
                </div>
              </div>
              <!-- ./col -->
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- VIEW MD -->
    <div class="modal fade" id="view_status_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Application status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body status-body">
          </div>
          <div class="modal-body">
            <div class="card-footer">
              <form id="chat_form_user" action="#" method="post" autocomplete="off">
                <div class="input-group">
                    <input type="text" name="message" placeholder="Type Message ..." class="form-control" required autofocus>
                    <span class="input-group-append">
                    <button type="submit" class="btn btn-outline-primary"><i class="far fa-paper-plane"></i></button>
                    </span>
                </div>
                </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_reports(1);


        $("#chat_form_user").on("submit", function(e){
            e.preventDefault();
            var uID = $("#uID").val();
            var type = $("#type").val();
            var typeID = $("#typeID").val();
            var data = $(this).serialize()+"&uID="+uID+"&type="+type+"&typeID="+typeID;

            var url = "../ajax/chat_add.php";
            $.ajax({
              type: "POST",
              url: url,
              data: data,
              success: function(data){
                if(data == 1){
                  window.location.reload();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
        });
      });

      function delete_chat(cID){

        var url = "../ajax/chat_delete.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            if(data == 1){
              window.location.reload();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      }

      function get_reports(filter){
        $("#tbl_reports").DataTable().destroy();
        $("#tbl_reports").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/report_data.php",
            "data": {filter: filter}
          },
          "processing": true,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.report_id+"' name='cb_reports' <?=$hide?>>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "user"
          },
          {
            "data": "address"
          },
          {
            "data": "description"
          },
          {
            "mRender": function(data, type, row){
              <?php if($_SESSION["role"] == 0){ ?>
                return "<a href='#' onclick='view_status(1,"+row.report_id+")'><u>View</u></a>";
              <?php }else{ ?>
                return row.status;
              <?php } ?>
            }
          },
          {
            "data": "priority"
          },
          {
            "data": "date_added"
          },
          {
            "mRender": function(data, type, row){
              if(row.status == "Cancelled"){
                var cancel = "<button class='btn btn-sm btn-outline-dark' onclick='edit_report("+row.report_id+")' <?=$hide?>>View Reason</button>";
              }else{  
                var cancel = "";
              }
              return "<button class='btn btn-sm btn-outline-dark' onclick='view_report("+row.report_id+")' <?=$hide_user?>>View Report</button>"+
              "<button class='btn btn-sm btn-outline-dark' onclick='edit_report("+row.report_id+")' <?=$hide?>>Edit Report</button>"+cancel
              ;
            }
          }
          ]
        });
      }

      function view_report(rID){
        window.location.href="index.php?page=<?=page_url('reports_view')?>&r_id="+rID;
      }

      function view_status(type, report_id){
        var url = "../ajax/view_status.php";
        $.ajax({
            type: "POST",
            url: url,
            data: {report_id: report_id, type: type},
            success: function(data){
              if(data){
                  $("#view_status_md").modal();
                  $(".status-body").html(data);
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
      }

      function checkAll(){
        var cb = $("#checkAll").is(":checked");
        if(cb){
          $("input[name=cb_reports]").prop("checked", true);
        }else{
          $("input[name=cb_reports]").prop("checked", false);
        }
      }

      $("#add_report_form").submit( function(e){
        e.preventDefault();
        var data = new FormData(this);
        var priority = 0;//$("input[name=prio]:checked").val();
        data.append('priority', priority);
        var url = "../ajax/report_add.php";
        $(".btn-primary").prop("disabled", true);
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data != 0){
              alert("Success: New report was added.");
              $("input[type=file]").val("");
              $("textarea").val("");
              $("#add_report_md").modal("hide");
              get_reports(1);
              $("#view_report_md").modal();
            }else{
              alert("Error: Something was wrong.");
            }
            $(".btn-primary").prop("disabled", false);
          }
        });
      });

      function edit_report(rID){
        var url = "../ajax/report_details.php";
        $(".img-container").html("");
        // $("input[type=checkbox]").prop("checked", false);
        $.ajax({
          type: "POST",
          url: url,
          data: {rID: rID},
          success: function(data){
            if(data){
              $("#edit_report_md").modal();
              var o = JSON.parse(data);
              $("#r_id").val(rID);
              $("#description").val(o.description);
              $("#address").val(o.address);
              if(o.img.length != 0){
                for(let x = 0; x < o.img.length; ++x){
                  $(".img-container").append('<img class="attachment_prev mb-3" src="'+o.img[x]+'" height="200" width="425">');
                }
              }else{
                $(".img-container").append('<h5 class="text-center">No Data Available.</h5>');
              }
            }
          }
        });
      }

      $("#edit_report_form").submit( function(e){
        e.preventDefault();
        var data = new FormData(this);
        var priority = 0;//$("input[name=prio]:checked").val();
        data.append('priority', priority);
        var url = "../ajax/report_update.php";
        $(".btn-primary").prop("disabled", true);
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          contentType: false,
          processData: false,
          success: function(data){
            if(data != 0){
              alert("Success: Report was updated.");
              $("input[type=file]").val("");
              $("textarea").val("");
              $("#edit_report_md").modal("hide");
              get_reports(1);
            }else{
              alert("Error: Something was wrong.");
            }
            $(".btn-primary").prop("disabled", false);
          }
        });
      });

      function delete_report(){
        var url = "../ajax/report_delete.php";
        var conf = confirm("Are you sure to delete report/s?");
        var rID = [];
        $("input[name=cb_reports]:checked").each( function(){
          rID.push($(this).val());
        });

        if(conf){
          $.ajax({
            type: "POST",
            url: url,
            data: {rID: rID},
            success: function(data){
              if(data != 0){
                alert("Success: Selected report/s was removed.");
                get_reports(1);
              }else{
                alert("Error: Something was wrong.");
              }
            }
          });
        }

      }

      function report_filter(){
        var filter = $("#rp_filter").val();
        get_reports(filter);
      }


      function cb1(){
        $("input[type=checkbox]").prop("checked", false);
        $(".cb1").prop("checked", true);
      }

      function cb2(){
        $("input[type=checkbox]").prop("checked", false);
        $(".cb2").prop("checked", true);
      }

      function cb3(){
        $("input[type=checkbox]").prop("checked", false);
        $(".cb3").prop("checked", true);
      }

      $("#prio1").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio1").prop("checked", true);
      });

      $("#prio2").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio2").prop("checked", true);
      });

      $("#prio3").click( function(){
        $("input[type=checkbox]").prop("checked", false);
        $("#prio3").prop("checked", true);
      });
      
    </script>