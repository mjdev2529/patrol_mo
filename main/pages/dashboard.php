<?php 

  $count_report = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM tbl_report"));
  $count_application = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM tbl_application"));
  $count_users = mysqli_num_rows(mysqli_query($conn,"SELECT * FROM tbl_users"));

  $hide = $_SESSION["role"] == 0?"style='display:none;'":"";

?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-2 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?>
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row" <?=$hide?>>
            <div class="col-lg-4 col-12">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_report?$count_report:0?></h3>

                  <p>Incident Reports</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="index.php?page=<?=page_url('reports')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-12">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_application?$count_application:0?></h3>

                  <p>Applications</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="index.php?page=<?=page_url('applications')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-12">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_users?$count_users:0?></h3>

                  <p>Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="index.php?page=<?=page_url('users')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="text-center">Welcome to PatrolMo: Barangay Issue Reporting</h3>
                </div>
                <p class="text-center col-8 offset-2 mt-5">
                  "PatrolMo: Barangay Issue Reporting App with toast notification” stands at the forefront of keeping the peace and order in the barangay.
                  This app will help people report an issue with a picture anytime and can let the sender's profile be anonymous for some personal concerns.
                  Once the reported problem is confirmed, it will be automatically filed in the barangay hall so that their employees will take action right away depending on the level of response.
                  A patrol activity is not passive, and must be evaluated on a continuous basis.
                  Reduction of violence, obsolescence, self-satisfaction and the preservation of values, information and best practices should be considered.
                </p>
                <!-- /.card-header -->
                <div class="card-body">
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
    </script>