<?php
  if($page == page_url('dashboard')){
    $dashboard = "active";
    $sales = "";
    $products = "";
    $reports = "";
    $applications = "";
  }else if($page == page_url('sales')){
    $dashboard = "";
    $sales = "active";
    $products = "";
    $reports = "";
    $applications = "";
  }else if($page == page_url('products')){
    $dashboard = "";
    $sales = "";
    $products = "active";
    $reports = "";
    $applications = "";
  }else if($page == page_url('users')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $users = "active";
    $reports = "";
    $applications = "";
  }else if($page == page_url('reports')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $reports = "active";
    $applications = "";
  }else if($page == page_url('applications')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $reports = "";
    $applications = "active";
  }
?>
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
  <li class="nav-header">NAVIGATION</li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('dashboard')?>" class="nav-link <?=$dashboard?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Dashboard
      </p>
    </a>
  </li>
  <!-- <li class="nav-header">MASTER DATA</li> -->
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('reports')?>" class="nav-link <?=$reports?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Incident Reports
        <?php 
          $count_rep = mysqli_num_rows(mysqli_query($conn,"SELECT report_id FROM tbl_report WHERE status = 0"));
          if($count_rep != 0 && $_SESSION["role"] == 1){
        ?>
          <span class="badge badge-info right">
            <?=$count_rep?>
          </span>
        <?php } ?>
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('applications')?>" class="nav-link <?=$applications?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Applications
        <?php 
          $count_app = mysqli_num_rows(mysqli_query($conn,"SELECT application_id FROM tbl_application WHERE status = 0"));
          if($count_app != 0 && $_SESSION["role"] == 1){
        ?>
          <span class="badge badge-info right">
            <?=$count_app?>
          </span>
        <?php } ?>
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('users')?>" class="nav-link <?=$users?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        User Management
      </p>
    </a>
  </li>
  <!-- <li class="nav-item">
    <a href="index.php?page=<?=page_url('products')?>" class="nav-link <?=$products?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Products
      </p>
    </a>
  </li> -->
  <!-- <li class="nav-header">TRANSACTIONS</li> -->
</ul>