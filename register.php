<?php include 'core/config.php'; ?>
<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>PatrolMo</title>

	  <!-- Google Font: Source Sans Pro -->
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
	  <!-- icheck bootstrap -->
	  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Theme style -->
	  <!-- CSS only -->
	  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">	
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="index.php"><b>Patrol</b>Mo</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form id="reg_form" action="#" method="post">
        <div class="form-group mb-3">
          <input type="text" class="form-control" placeholder="Full name" name="pname">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control" placeholder="Contact" name="contact">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-1">
          <input type="email" class="form-control" placeholder="Email address" name="email">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-3">
          <small>Address in Villamonte</small>
          <select class="form-control" name="address">
            <option value="0">Select:</option>
            <?php
              $get_address = mysqli_query($conn, "SELECT * FROM tbl_address ORDER BY address");
              while($row = mysqli_fetch_array($get_address)){
            ?>
              <option value="<?=$row['address_id']?>"><?=$row['address']?></option>
            <?php } ?>
          </select>
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-home"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="uname">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="pass">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div> -->
        </div>
        <div class="form-group mb-3">
          <input type="password" class="form-control" placeholder="Retype password" name="pass2">
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div> -->
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4 offset-8">
            <button type="submit" class="btn btn-outline-danger btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="login.php" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>
<script>
    $("#reg_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "ajax/register.php";

        $.ajax({
            "type": "POST",
            "url": url,
            "data": data,
            "success": function(data){
                if(data == 1){
                    alert("Success: New account was registered.");
                    window.location.href="index.php";
                }else{
                    alert("Error: something was wrong.");
                }
            }
        });
    });
</script>
</body>